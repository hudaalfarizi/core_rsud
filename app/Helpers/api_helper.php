<?php

function sendResponse($code, $data='Data tidak ditemukan'){
    $status = $code == 200 ? 'success' : 'failed';

    if ($status == 'success') {
        $response = array(
            "code" => $code,
            "status" => $status,
            "data" => $data
        );
    }
    else{
        $response = array(
            "code" => $code,
            "status" => $status,
            "msg" => $data
        );
    }

    return $response;
}

?>