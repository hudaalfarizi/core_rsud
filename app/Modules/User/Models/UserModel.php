<?php 
namespace App\Modules\User\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $db;
    protected $useTimestamps = true;

    public function __construct()
    {
        helper('api');
        $this->db              = \Config\Database::connect();
        $this->users           = $this->db->table('public.users');
        $this->user_accounts   = $this->db->table('public.user_accounts');
    }

    public function getData($where = null)
    {
        $table = $this->db->table('users u');
        
        if ($where !== NULL) {
            $table->where($where);
        }

        $data = $table->select('
                                u.*,
                                uc.name,
                                uc.nik,
                                uc.email,
                                uc.phone,
                                uc.image')
                      ->join('user_accounts uc', 'uc.id = u.useraccount_id')
                      ->get()
                      ->getResultArray();

        if (count($data)) {	

            return sendResponse(200, $data);
            
		} else {

            return sendResponse(404);
            
		}
    }

    public function tambahData()
    {
        $userID = 1;

        $email_cek = $this->user_accounts->getWhere(['email'=>$_POST['email']])->getRowArray();
        
        if($email_cek)
        {

            $data = ['msg'=>'email sudah terdaftar'];
            return sendResponse(500,$data);

        } else {

            $uname_cek = $this->users->getWhere(['username'=>$_POST['username']])->getRowArray();
            if($uname_cek)
            {

                $data = ['msg'=>'username sudah terdaftar'];
                return sendResponse(500,$data);

            } else {

                $insert1 = array(
                    'name'       => $_POST['name'],
                    'email'      => $_POST['email'],
                    'nik'        => $_POST['nik'],
                    'phone'      => $_POST['phone'],
                    'created_by' => $userID,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => $userID
                );

                $this->db->transStart();
                $this->user_accounts->insert($insert1);
                        $get_id = $this->user_accounts->getWhere(['email'=>$_POST['email']])->getRowArray();

                        $insert2 = array(
                                    'username'       => $_POST['username'],
                                    'password'       => $_POST['password'],
                                    'useraccount_id' => $get_id['id'],
                                    'role_id'        => $_POST['role_id'],
                                    'unit_id'        => $_POST['unit_id'],
                                    'roomcategory_id'=> $_POST['roomcategory_id'],
                                    'created_at'     => date('Y-m-d H:i:s'),
                                    'created_by'     => $userID,
                                    'updated_at'     => date('Y-m-d H:i:s'),
                                    'updated_by'     => $userID,
                                    'status'         => $_POST['status']
                                );
                        
                $this->users->insert($insert2);
                $this->db->transComplete();
                if ($this->db->transStatus() === FALSE)
                {
                    $this->db->transRollback();

                    $data = ['msg'=>'gagal tambah data'];
                    return sendResponse(500,$data);

                } else {

                    $this->db->transCommit();

                    $data = ['msg'=>'berhasil tambah data'];
                    return sendResponse(200,$data);

                }

            }
        }

    }

    public function updateData($table, $data, $where)
    {
        $exec = $this->db->table($table)->update($data, $where);

        if ($exec) {
            return true;
        }

        return false;
    }

    public function deleteData($table, $where)
    {
        $exec = $this->db->table($table)->delete($where);

        if ($exec) {
            return true;
        }

        return false;
    }

    public function get($table, $where=NULL)
    {
        $tbl = $this->db->table($table);

        if ($where !== NULL) {
            $tbl->where($where);
        }

        return $tbl->get()->getResultArray();
    }
}