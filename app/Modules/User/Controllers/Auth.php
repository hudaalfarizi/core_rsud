<?php 
namespace App\Modules\User\Controllers;
use App\Controllers\BaseController;

use App\Modules\User\Models\UserModel;

class Auth extends BaseController
{
    protected $userModel;
    
    public function __construct()
    {
        $this->userModel = new UserModel();
    }

    public function index()
    {
        $data['title'] = 'Masuk ke Dashboard CS Unit Ruangan';
        return view('auth', $data);
    }

    public function act_login()
    {
        $post = $this->request->getVar();

        $where = [
            'username' => $post['username'],
            'password' => $post['password']
        ];

        $data = $this->userModel->getData($where);

        print_r($data);

        // if ($auth['res'] == TRUE) {
		// 	$this->session->set_userdata('rsud_cs_ruangan', $auth['data']);
		// 	$feedback['res'] = "success";
		// 	$feedback['msg'] = "Berhasil Login";
		// 	$feedback['url'] = site_url('cs_ruangan');
		// } else {
		// 	$feedback['url'] = site_url('cs_ruangan/authenticate');
		// 	$feedback['res'] = "fail";
		// 	$feedback['msg'] = "Login gagal, username atau kata sandi salah!";
		// }
		// echo json_encode($feedback);
    }

}