<?php namespace App\Modules\User\Controllers;

use App\Controllers\BaseController;
use App\Modules\User\Models\UserModel;
use CodeIgniter\API\ResponseTrait;


class User extends BaseController
{
	use ResponseTrait;
	
	public function __construct()
	{
		//lib
		$this->validation = \Config\Services::validation();

		//model
		$this->userModel = new UserModel();

		// $this->userAccountModel = new UserAccountModel();	
	}

	public function findAll()
	{
		$data = $this->userModel->getData();
		return $this->respond($data);
	}

	public function findById($id)
	{
		$where = array(
			'u.id' => $id
		);
		
		$data = current($this->userModel->getData($where));
		return $this->respond($data);
	}

	public function add()
	{
		
		$userID = 1;
		
		$this->validation->setRules([
			'name' 	          => 'required',
			'email'           => 'required|valid_email',
			'nik'	          => 'required',
			'phone'	          => 'required|numeric',
			'role_id'         => 'required|numeric',
			'unit_id'         => 'required|numeric',
			'roomcategory_id' => 'required|numeric',
			'username'        => 'required',
			'password'        => 'required',
			'status'          => 'required'
		]);

		if($this->validation->withRequest($this->request)->run() === FALSE )
        {
            $data = [
					 'res'=>'fail',
					 'msg'=>$this->validation->listErrors()
			];

			return $this->respond($data,200);
			
        } else {
			$data = $this->userModel->tambahData();
			return $this->respond($data);
		}
		
	}











	public function update($id)
	{
		$data = $this->body;
		$userID = 1;

		$dataUpdated = Array (
			'username' => $data['username'],
			'password' => $data['password'],
			'unit_id' => $data['unit_id'],
			'role_id' => $data['role_id'],
			'updated_at' => date('Y-m-d H:i:s'),
			'updated_by' => $userID,
			'status' => 1
		);
		
		$updateUser = $this->userModel->updateData('users',$dataUpdated, ['id'=>$id]);
		$accountID = '';
		if ($updateUser) {
			$user = $this->userModel->get('users', ['id'=>$id]);
			$accountID = $user[0]['useraccount_id'];
		}

		$dataUpdated = Array (
			'name' => $data['name'],
			'email' => $data['email'],
			'nik' => $data['nik'],
			'phone' => $data['phone'],
			'updated_at' => date('Y-m-d H:i:s'),
			'updated_by' => $userID
		);
		$userccount_id = $this->userModel->updateData('user_accounts',$dataUpdated, ['id'=>$accountID]);

		$res = sendResponse(200, 'Data Berhasil Diupdate');
		return $this->respond($res);
	}

	public function updatePassword($id)
	{
		$data = $this->body;
		$insert1 = Array (
					'password' => $data['password'],
					'updated_at' => date('Y-m-d H:i:s'),
					'updated_by' => $id
				);
		$this->userModel->updateData('users',$insert1, ['id'=>$id]);

		$res = sendResponse(200, 'Data Berhasil Diupdate');
		return $this->respond($res);
	}

	function delete($id)
	{
		$dataAccount = $this->userModel->getData(['u.id'=>$id])[0];

		$deleteAccount = $this->userModel->deleteData('user_accounts', ['id'=>$dataAccount['useraccount_id']]);
		$deleteUser = $this->userModel->deleteData('users', ['id'=>$id]);

		
		if ($deleteUser) {
			$status = array('status'=>'success', 'msg'=>'Berhasil Menghapus Data');
			$res = sendResponse($status);
		}
		else{
			$res = sendResponse(500, 'Gagal Menghapus Data');
		}

		return $this->respond($res);
	}

	// public function validasi()
	// {
	// 	$data = $this->body;
		
	// 	// validasi untuk username
	// 	$user = $this->userModel->get('users',['username'=>$data['username'], 'id !='=>$id]);
	// 	if (count($user)) {
	// 		$res = $this->fail(500, 'Username Tidak Boleh Sama');
	// 	}

	// 	// validasi untuk nik
	// 	$isExist = $this->userModel->get('user_accounts', ['nik'=>$data['nik'], 'id !='=> $user[0]['useraccount_id']]);
	// 	if (count($isExist)) {
	// 		$res = $this->fail(500, 'Nik Tidak Boleh Sama');
	// 	}
	// }
	

	//--------------------------------------------------------------------

}
